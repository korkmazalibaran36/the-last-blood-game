﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    public Animator bulletanim;
    public bool attack, degdi;
    public int b=0, para;
    public float a = 2f;

    void Start()
    {
        para = PlayerPrefs.GetInt("para");

    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            bulletan();
        }
        if (degdi)
        {
            b--;
            if(b == 0)
            {
                this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
                this.gameObject.GetComponent<SpriteRenderer>().enabled = true;

                degdi = false;
            }
        }
        if (attack)
        {
            a--;
            if (a == 0)
            {
                attack = false;
            }
        }
    }
    public void bulletan()
    {
        a = 90;
        attack = true;
        bulletanim.Play("bullet");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.name == "zombie" || collision.gameObject.name == "ninja")
        {
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = false;

            b = 14; 
            degdi = true;
            para += 10;
            PlayerPrefs.SetInt("para", para);
        }
    }
}

