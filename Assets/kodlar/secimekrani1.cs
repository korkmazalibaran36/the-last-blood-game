﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class secimekrani1 : MonoBehaviour
{

    public int level;
    public GameObject ago, bgo, cgo, dgo, ego;

    void Start()
    {
        
        level = PlayerPrefs.GetInt("level");


        if (level < 15)
        {
            ago.gameObject.SetActive(true);
            bgo.gameObject.SetActive(false);
            cgo.gameObject.SetActive(false);
            dgo.gameObject.SetActive(false);
            ego.gameObject.SetActive(false);

        }
        else if (level >= 15 && level < 30)
        {
            ago.gameObject.SetActive(false);
            bgo.gameObject.SetActive(true);
            cgo.gameObject.SetActive(false);
            dgo.gameObject.SetActive(false);
            ego.gameObject.SetActive(false);

        }
        else if (level >= 30 && level < 41)
        {
            ago.gameObject.SetActive(false);
            bgo.gameObject.SetActive(false);
            cgo.gameObject.SetActive(true);
            dgo.gameObject.SetActive(false);
            ego.gameObject.SetActive(false);

        }
        else if (level >= 41 && level < 50)
        {
            ago.gameObject.SetActive(false);
            bgo.gameObject.SetActive(false);
            cgo.gameObject.SetActive(false);
            dgo.gameObject.SetActive(true);
            ego.gameObject.SetActive(false);

        }
        else if (level > 50)
        {
            ago.gameObject.SetActive(false);
            bgo.gameObject.SetActive(false);
            cgo.gameObject.SetActive(false);
            dgo.gameObject.SetActive(false);
            ego.gameObject.SetActive(true);

        }
    }

    void Update()
    {
      
    }
}
