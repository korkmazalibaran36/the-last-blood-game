﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class levelsecim : MonoBehaviour
{
    public int level, a;

    void Start()
    {
        level = PlayerPrefs.GetInt("level");
    }

    void Update()
    {
        if (level >= a)
        {
            this.gameObject.GetComponent<Button>().interactable = true;
        }
        else
        {
            this.gameObject.GetComponent<Button>().interactable = false;

        }
  

    }
}
