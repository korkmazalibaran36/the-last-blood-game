﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kontrol : MonoBehaviour
{

    public GameObject[] butonlar;
    public GameObject karakter, kalp1,kalp2,kalp3,kalp4 ,reklambuton , failpanel,mektupimg;
    public bool arka, ontr,rotasyon, ziplabool,attack,bll, cankontrol,can2,bulletattack;
    public Animator karakteranim;
    public int a, ziplaint=60,  level, levelkontrol,bulletint, ses;
    public float krktrcan = 100f;
    int b = 60;
    bool bbl = false;


    void Start()
    {

        level = PlayerPrefs.GetInt("level");
        ontr = false;
        arka = false;
    }

    void FixedUpdate()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            yurumegeri();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            yurumeileri();
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            idle();
            arka=false;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            idle();
            ontr = false;
        }
        if(Input.GetKeyDown(KeyCode.LeftShift)|| Input.GetKeyDown(KeyCode.RightShift))
        {
            zipla();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            saldir();
        }
        
        ses = PlayerPrefs.GetInt("ses");

        if (ses == 0)
        {
            GameObject.Find("ses").GetComponent<AudioSource>().volume = 1;
            GameObject.Find("ses1").GetComponent<AudioSource>().volume = 1;

        }
        if (ses == 1)
        {
            GameObject.Find("ses").GetComponent<AudioSource>().volume = 0;
            GameObject.Find("ses1").GetComponent<AudioSource>().volume = 0;

        }
        bulletint = GameObject.Find("bullet").GetComponent<bullet>().b;
        cankontrol = reklambuton.GetComponent<PlayUnityAd>().cankontrol;

        if(krktrcan> 0)
        {
            can2 = true;


        }
     
        bulletattack = GameObject.Find("bullet").GetComponent<bullet>().attack;
        if (bbl)
        {
            b--;
            if (b == 0)
            {
                Application.LoadLevel(6);
            }
        }
        karakter = this.gameObject;
        if (cankontrol)
        {
            krktrcan = 50.0f;
     
        }
        if (krktrcan == 75.0f || krktrcan == 62.5f)
        {
            kalp4.gameObject.SetActive(false);
            kalp3.gameObject.SetActive(true);
            kalp2.gameObject.SetActive(true);
            kalp1.gameObject.SetActive(true);

        }
        if (krktrcan == 50.0f || krktrcan == 37.5f)
        {
            kalp4.gameObject.SetActive(false);
            kalp3.gameObject.SetActive(false);
            kalp2.gameObject.SetActive(true);
            kalp1.gameObject.SetActive(true);

        }
        if (krktrcan == 25.0f || krktrcan == 12.5f)
        {

            kalp4.gameObject.SetActive(false);
            kalp3.gameObject.SetActive(false);
            kalp2.gameObject.SetActive(false);
            kalp1.gameObject.SetActive(true);
        }
        if (krktrcan <= 0.0f)
        {

            can2 = false;
            kalp4.gameObject.SetActive(false);
            kalp3.gameObject.SetActive(false);
            kalp2.gameObject.SetActive(false);
            kalp1.gameObject.SetActive(false);

            karakteranim.Play("girldead");
            butonlar[0].gameObject.SetActive(false);
            butonlar[1].gameObject.SetActive(false);
            butonlar[2].gameObject.SetActive(false);
            butonlar[3].gameObject.SetActive(false);
            butonlar[4].gameObject.SetActive(false);
            if (!bbl)
            {
                failpanel.gameObject.SetActive(true);
            }

        }
        if (ziplabool)
        {
            ziplaint--;
            karakter.transform.Translate(0, 0.18f, 0);
            if (ziplaint == 0)
            {
                ziplabool = false;
            }
        }
        if (krktrcan > 0)
        {
            if (!rotasyon)
            {
                if (ontr)
                {
                    if (!ziplabool )
                    {
                        karakteranim.Play("girlrun");
                    }
                    karakter.transform.Translate(0.15f, 0, 0);
                }
                if (arka)
                {
                    if (!ziplabool)
                    {
                        karakteranim.Play("girlrun");
                    }
                    karakter.transform.Translate(-0.15f, 0, 0);
                }
            }
            if (rotasyon)
            {
                if (ontr)
                {
                    if (!ziplabool)
                    {
                        karakteranim.Play("girlrun");
                    }
                    karakter.transform.Translate(-0.15f, 0, 0);
                }
                if (arka)
                {
                    if (!ziplabool)
                    {
                        karakteranim.Play("girlrun");
                    }
                    karakter.transform.Translate(0.15f, 0, 0);
                }
            }
        }
    }
    public void yurumeileri()
    {
        if (a==1) {
            rotate();
            arka = false;
        } 
        ontr = true;
        a = 0;
        
     
    }
    public void yurumegeri()
    {
      if (a==0)
        {
            rotate();
            ontr = false;
        }
        arka = true;
        a = 1;
       

    }
    public void rotate()
    {
        karakter.transform.Rotate(0,180, 0);
        if (rotasyon)
        {
            rotasyon = false;
        }
        else if (!rotasyon)
        {
            rotasyon = true;
        }

    }
        public void idle()
        {
            karakteranim.Play("girlidle");
        ontr = false;
        arka = false;
    }
    public void zipla()
    {
        ziplabool = true;
        ziplaint = 60;
        karakteranim.Play("girljump");
    }
    public void saldir()
    {
        if (bulletint == 0)
        {
            karakteranim.Play("shoot");
            attack = true;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        bool attack;

        if (collision.gameObject.name == "zombie")
        {
            attack = collision.GetComponent<zombie>().attack;

            if (attack)
            {

            }
            bll = collision.gameObject.GetComponent<zombie>().bll;
            if (!bll && !bulletattack)
            {
                krktrcan -= 12.5f;
            }
        }
        if (collision.gameObject.name == "ninja")
        {
            attack = collision.GetComponent<ninja>().attack;

            if (attack)
            {

            }
            bll = collision.gameObject.GetComponent<ninja>().bll;
            if (!bll && !bulletattack)
            {
                krktrcan -= 12.5f;
            }
        }
        if (collision.gameObject.name == "robot")
        {
            attack = collision.GetComponent<robot>().attack;

            if (attack)
            {

            }
            bll = collision.gameObject.GetComponent<ninja>().bll;
            if (!bll && !bulletattack)
            {
                krktrcan -= 12.5f;
            }
        }
        if (collision.gameObject.name == "robobullet")
        {
        
                krktrcan -= 12.5f;
           
        }
            if (collision.gameObject.name == "diken")
        {

            krktrcan = 0f;
            bbl = true;
            failpanel.gameObject.SetActive(false);
        }
        if (collision.gameObject.name == "finish")
        {
            if (levelkontrol > level)
            {
                level += 1;
                PlayerPrefs.SetInt("level", level);
                Application.LoadLevel(7);

            }
            else
            {
                Application.LoadLevel(7);

            }
        }
        if (collision.gameObject.name == "bombbullet")
        {
            krktrcan -= 12.50f;

        }
        if (collision.gameObject.name == "mektup")
        {
            mektupimg.gameObject.SetActive(true);
            collision.gameObject.SetActive(false);

        }
        if (collision.gameObject.name == "balyoz")
        {

            krktrcan -= 12.5f;
        }

    }
}

