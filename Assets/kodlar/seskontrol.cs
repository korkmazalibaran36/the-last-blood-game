﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class seskontrol : MonoBehaviour
{
    public int ses;
    public GameObject off, on;

    void Start()
    {
        
    }

    void Update()
    {
        ses = PlayerPrefs.GetInt("ses");

        if (ses == 0) {
            on.gameObject.SetActive(true);
            off.gameObject.SetActive(false);
        }
        if (ses == 1)
        {
            on.gameObject.SetActive(false);
            off.gameObject.SetActive(true);
        }
    }
    public void seson()
    {
        ses = 0;
        PlayerPrefs.SetInt("ses", ses);
    }
    public void sesoff()
    {
        ses = 1;
        PlayerPrefs.SetInt("ses", ses);

    }
}
