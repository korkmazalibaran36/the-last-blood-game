﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ninja : MonoBehaviour
{

    public bool attack;
    public Animator ninjaanim;
    public bool bll = false;
    public Transform karakter;
    public float fark;
    public int ninjacan;
    public GameObject canbar;
    public Vector3 scalechange;

    void Start()
    {
        ninjacan = 3;
        attack = false;
    }

    void FixedUpdate()
    {
            if (ninjacan > 0)
        {
            transform.Translate(0.10f, 0, 0);
        }

        if (bll == false || ninjacan < 0)
        {
            if (fark <= 1.5f)
            {
                ninjaanim.Play("ninjaattack");
            }
        }
        fark = Vector2.Distance(transform.position, karakter.position);
        if (ninjacan <= 0)
        {
            canbar.gameObject.SetActive(false);
            ninjaanim.Play("ninjadead");
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
 
     

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "bullet")
        {

            attack = true;
            ninjacan -= 1;
            canbar.transform.localScale += scalechange;



            if (fark >= 1.5f)
            {
                bll = true;
            }
        }
        if (collision.gameObject.name == "yan")
        {
            this.gameObject.transform.Rotate(0, 180, 0);
        }
        if (collision.gameObject.tag == "Karakter")
        {
            if (bll == false)
            {

            }
        }
    }

}
