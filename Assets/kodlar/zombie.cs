﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombie : MonoBehaviour
{

    public bool attack, attack2;
    public Animator zombianim;
    public bool bll=false;
    public Transform karakter;
    public float fark;
    public int a = 40;
    
    void Start()
    {
        attack = false;
    }

    void FixedUpdate()
    {
        if (bll == false)
        {
            transform.Translate(0.02f, 0, 0);
            if (fark <= 1)
            {
                zombianim.Play("zombieattack");
            }
        }
        fark = Vector2.Distance(transform.position, karakter.position);
        if (attack2)
        {
            a--;
        }

        else
        {
            a = 40;
        }
    
        if(a== 0)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "bullet")
        {

            attack = true;
            attack2 = true;

            Debug.Log("dokundun");
            zombianim.Play("Zombiedead");


            if (fark >= 1)
            {
                bll = true;
            }
        }
        if(collision.gameObject.name == "yan")
        {
            this.gameObject.transform.Rotate(0, 180, 0);
        }
        if (collision.gameObject.tag == "Karakter")
        {
            if(bll == false)
            {

            }
        }
    }
   
}
